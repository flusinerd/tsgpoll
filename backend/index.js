var express = require('express')
var app = express()
var SSE = require('express-sse')
var sse = new SSE();


class Poll {
    constructor (title) {
        this.yesCount = 0;
        this.noCount = 0;
        this.title = title;
        this.result = 'Unentschieden';
    }

    addYes() {
        this.yesCount++;
        this.getResult()
    }

    addNo() {
        this.noCount++;
        this.getResult()
    }

    getResult() {
        if (this.yesCount > this.noCount) {
            this.result = 'Ja';
        } else if (this.yesCount < this.noCount){
            this.result = 'Nein';
        }
        else {
            this.result = 'Unentschieden';
        }
        return this.result;
    }
}

var polls = [new Poll('Testpoll')];
polls[0].addYes();
console.log(polls);

app.get('/stream', sse.init);
 
app.get('/polls/:id', function (req, res) {
    if (polls[req.params.id]){
        res.json(polls[req.params.id])
    }
    else {
        res.status(404).send();
    }
})

app.post('/polls/:id', function (req, res) {
    if (polls[req.params.id]){
        if (req.body.choice){
            if (req.body.choice == 0){
                polls[req.params.id].addNo();
            } else {
                polls[req.params.id].addYes();
            }
        }
    res.status(200).send();
    sse.send(polls[req.params.id], req.params.id);
    }
})
 
app.listen(3000)